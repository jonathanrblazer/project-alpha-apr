from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from projects.models import Project
from projects.forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {"project_list": project_list}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    this_project = Project.objects.get(pk=id)
    context = {"this_project": this_project}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    errors = {}
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
        else:
            errors = form.errors
    else:
        form = ProjectForm()

    users = User.objects.all()
    context = {"form": form, "users": users, "errors": errors}
    return render(request, "projects/create_project.html", context)
