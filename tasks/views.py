from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from tasks.forms import TaskForm
from tasks.models import Task
from projects.models import Project

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    projects = Project.objects.all()
    assignees = User.objects.all()
    context = {"form": form, "projects": projects, "assignees": assignees}

    return render(request, "projects/create_task.html", context)


@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": my_tasks}
    return render(request, "projects/show_my_tasks.html", context)
