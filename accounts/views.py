from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import AccountLoginForm, AccountSignupForm

# Create your views here.


def login_user(request):
    if request.method == "POST":
        form = AccountLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = AccountLoginForm()

    context = {"form": form}

    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect("login")


def signup_user(request):
    if request.method == "POST":
        form = AccountSignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                signed_up_user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, signed_up_user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = AccountSignupForm

    context = {"form": form}

    return render(request, "accounts/signup.html", context)
